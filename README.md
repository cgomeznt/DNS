## DNS

* [DNS Local en GNU/Linux](guia/dns-local-linux.rst)
* [DNS Publico en GNU/Linux](guia/dns-publico-linux.rst)
* [Configurar BIND en una Red Privada en Centos 7](guia/dns-local-centos7.rst)
* [Configurar BIND en una Red Privada con sub-dominio en Centos 7](guia/dns-local-subdominio-centos7.rst)
* [Activar los LOGs para BIND9](guia/activarlogs.rst)






